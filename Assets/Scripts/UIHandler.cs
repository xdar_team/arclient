﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.EventSystems;

public class UIHandler : MonoBehaviour {

	public GameObject DrawingCanvas, MainCanvas, MainImagePanel, MainThumbnailPanel;
	public GameObject openGalleryBtn, closeGaleryBtn;
	public GameObject ThumbnailButtonPrefab;
	public Image MainImage;
	public RectTransform SrollContent;

	private MeetingScreenshots meeting_screenshots;
	private List<Texture2D> screenshots_imgs;

	private Queue<int> new_images;

	int gallery_exists = 0;
	bool gallery_update = false;
	float thumbPanelWidth, thumbPanelHeigth, mainImagePanelWidth, mainImagePanelHeigth;
	IEnumerator checkGalleryUpdatesCoroutine;
	DeviceOrientation deviceOrientation;
	int current_image_index;
	void Start () {

		log("STARTED UIHanlder");

		screenshots_imgs = new List<Texture2D>();
		new_images = new Queue<int>();

		current_image_index = 0;

		openGalleryBtn.GetComponent<Button>().onClick.AddListener(() => openGallery());
		closeGaleryBtn.GetComponent<Button>().onClick.AddListener(() => closeGallery());

		deviceOrientation = Input.deviceOrientation;

		StartCoroutine(fetchMeetingScreenshots());

		checkGalleryUpdatesCoroutine = checkUpdateForGallery(1.0f);
	}
	
	void Update () {
		if(Input.GetKeyDown("a")){
			addImageToGallery(0);
		}

		if(Input.deviceOrientation != deviceOrientation){

			deviceOrientation = Input.deviceOrientation;
			updatePanelsSize();
		}
	}

	public void updatePanelsSize(){
		
		mainImagePanelWidth = MainImagePanel.GetComponent<RectTransform>().rect.width;
		mainImagePanelHeigth = MainImagePanel.GetComponent<RectTransform>().rect.height;
	}

	public void openGallery(){

		MainCanvas.SetActive(false);
		DrawingCanvas.SetActive(true);

		mainImagePanelWidth = MainImagePanel.GetComponent<RectTransform>().rect.width;
		mainImagePanelHeigth = MainImagePanel.GetComponent<RectTransform>().rect.height;
		thumbPanelWidth = MainThumbnailPanel.GetComponent<RectTransform>().rect.width;
		thumbPanelHeigth = MainThumbnailPanel.GetComponent<RectTransform>().rect.height;

		if(screenshots_imgs.Count > 0){
			
			log("Setting current image");
			changeMainImage(current_image_index);
		}

		log("IMAGES IN QUEUE: " + new_images.Count);
        StartCoroutine(checkGalleryUpdatesCoroutine);
	}

	public void closeGallery(){

		DrawingCanvas.SetActive(false);
		MainCanvas.SetActive(true);

		StopCoroutine(checkGalleryUpdatesCoroutine);
	}

	public IEnumerator checkUpdateForGallery(float waitTime)
    {
        while (true)
        {
			yield return new WaitForSeconds(waitTime);
            if(new_images.Count > 0){

				for(int i=0; i< new_images.Count; i++){

					int image_index = new_images.Dequeue();
					addImageToGallery(image_index);
				}
			}
        }
    }

	public void addImageToGallery(int image_index){

		log("ADD NEW IMAGE: " + image_index);

		GameObject newThumbnailButton = (GameObject)Instantiate(ThumbnailButtonPrefab);
		newThumbnailButton.transform.SetParent(SrollContent);

		newThumbnailButton.GetComponent<Button>().onClick.AddListener(() => changeMainImage(image_index, true));

		Texture2D thumnailImage = screenshots_imgs[image_index];
		float thumbWidth = thumnailImage.width;				
		float thumbHeigth = thumnailImage.height;	

		Rect rec = new Rect(0, 0, thumbWidth, thumbHeigth);
		Sprite spriteToUse = Sprite.Create(thumnailImage, rec, new Vector2(0.5f,0.5f),100);

		float width, heigth;
		width = thumbPanelWidth;
		heigth = width * (thumbHeigth/thumbWidth);
		
		newThumbnailButton.GetComponent<RectTransform>().sizeDelta = new Vector2(width, heigth);
		newThumbnailButton.GetComponent<Image>().sprite = spriteToUse;
	}

	public void changeMainImage(int index, bool notify_others = false){

		log("CHANGE MAIN IMAGE TO " + index);
		if(index != current_image_index){
			
			current_image_index = index;
			float newImageWidth = screenshots_imgs[index].width;
			float newImageHeigth = screenshots_imgs[index].height;

			Rect rec = new Rect(0, 0, newImageWidth , newImageHeigth);
			Sprite spriteToUse = Sprite.Create(screenshots_imgs[index],rec,new Vector2(0.5f,0.5f),100);		

			float width, heigth;
			if(newImageWidth > newImageHeigth){

				//heigth = newImageHeigth * (width/newImageWidth);
				width = mainImagePanelWidth;
				heigth = width * (newImageHeigth/newImageWidth);
			}
			else{
				
				heigth = mainImagePanelHeigth;
				width = heigth * (newImageWidth/newImageHeigth);
			}

			Debug.Log(newImageWidth+ " " +newImageHeigth);
			Debug.Log(width+ " " +heigth);

			MainImage.rectTransform.sizeDelta = new Vector2(width, heigth);
			MainImage.sprite = spriteToUse;
		}else{
			log("Image already displayed");
		}

		//inform server about update
		if(notify_others){
				
				ImageUpdate imgUpdateMsg = new ImageUpdate("change_image", current_image_index);

				string json_msg = JsonConvert.SerializeObject(imgUpdateMsg);
				this.GetComponent<NetworkTablet>().WsSendMessage(json_msg);
			}
	}

	private IEnumerator fetchMeetingScreenshots(){

		log("DOWNLOADING MEETING SCREENSHOTS LIST");

		UnityWebRequest wwwList = UnityWebRequest.Get(Constants.HttpHost + "/api/meeting/screenshots/" + Constants.MeetingID);
        yield return wwwList.SendWebRequest();

		string list = wwwList.downloadHandler.text;

		meeting_screenshots = JsonConvert.DeserializeObject<MeetingScreenshots>(list);

		current_image_index = meeting_screenshots.current_image;

		log("DOWNLOADING SCREENSHOTS ON LIST. current_image_index" + current_image_index);

		foreach(string url in meeting_screenshots.urls)
			yield return downloadScreenshot(url);

		log("MEETING SCREENSHOTS DOWNLOAD COMPLETED: " + screenshots_imgs.Count.ToString());		
	}

	public IEnumerator downloadScreenshot(string url, bool update_main_image = false){
		
		log("D_IMG:  " + url);
		UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
		yield return www.SendWebRequest();

		if(www.isNetworkError || www.isHttpError) log("ERROR DOWNLOADING IMAGE" + www.error);
		else {

			screenshots_imgs.Add(((DownloadHandlerTexture)www.downloadHandler).texture);

			int image_index = screenshots_imgs.Count > 0 ? screenshots_imgs.Count -1 : 0;

			new_images.Enqueue(image_index);

			if(update_main_image)
				changeMainImage(image_index);
		
		}
		www.Dispose();
		www = null;	
	}

	public void log(string msg){
        Debug.Log("DEBUG - " + msg);
    }
}
