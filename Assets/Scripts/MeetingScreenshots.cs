public class MeetingScreenshots
{
    public string[] urls { get; set; }
    public int current_image { get; set; }

    public MeetingScreenshots(string[] urls, int current_image){
        this.urls = urls;
        this.current_image = current_image;
    }
}