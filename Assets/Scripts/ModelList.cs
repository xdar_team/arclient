public class ModelList
{
    public string[] Names { get; set; }
    public string[] Uuids { get; set; }
    public string[] Scales { get; set; }

    public ModelList( string[] names, string[] uuids, string[] scales)
    {
        Names = names;
        Uuids = uuids;
        Scales = scales;
    }
}