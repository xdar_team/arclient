public class WSUpdate{

    public string Type { get; set;}
}

public class ModelUpdate
{
    public string Update { get; set; }
    public string Uuid { get; set; }
    public string Name { get; set; }
    public string Scale { get; set; }
    public string Target { get; set; }
}
public class WSScreenshot : WSUpdate{

    public string[] urls {get; set;} 
}