public static class Constants{

    public static string HttpHost = "https://xdar-au.herokuapp.com";
    public static string WSHost = "wss://xdar-au.herokuapp.com";
    
    // public static string HttpHost = "http://localhost:8100";
    // public static string WSHost = "ws://localhost:8100";

    public static string MeetingID = "5bebef457c400accf534af8d";
    public static int NumberOfTargets = 3;
}