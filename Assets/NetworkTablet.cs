﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using WebSocketSharp;
using Vuforia;
using UnityEngine.UI;

public class NetworkTablet : MonoBehaviour
{
    public Light directional_light;
    public GameObject MainCanvas;
    public WebSocket wsClient;
    List<String> trackedImageTargets;
    public readonly static Queue<Action> ExecuteOnMainThread = new Queue<Action>();
    public ModelList currentList;
    
    void Start()
    {
        trackedImageTargets = new List<string>();

        //Function initialization
        InitializeCurrentList();
        CreateWebSocketSession();
        StartCoroutine(GetAllModels());
    }

    void Update()
    {
        // dispatch stuff on main thread
        while (ExecuteOnMainThread.Count > 0)
        {
            ExecuteOnMainThread.Dequeue().Invoke();
        }

        if(Input.GetKeyDown("s")){
            
            StartCoroutine(takeScreenshot());
        }
    }

    void OnApplicationQuit(){

        log("AR CLIENT CLOSED");
        //wsClient.Send("CLOSING AR CLIENT APP");
        wsClient.Close(CloseStatusCode.Normal, "app closed");
    }
    void InitializeCurrentList()
    {
        if (File.Exists(System.IO.Path.Combine(Application.persistentDataPath, "currentList.txt"))) 
        {
            log(System.IO.Path.Combine(Application.persistentDataPath, "currentList.txt"));

            string listContent = File.ReadAllText(System.IO.Path.Combine(Application.persistentDataPath, "currentList.txt"));

            currentList = JsonConvert.DeserializeObject<ModelList>(listContent);
        }
        else
        {
            log("FILE NOT FOUND: Creating new");
            currentList = new ModelList(new string[Constants.NumberOfTargets], new string[Constants.NumberOfTargets], new string[Constants.NumberOfTargets]);
            for (int i = 0; i < currentList.Uuids.Length; i++)
            {
                currentList.Uuids[i] = "0";
                currentList.Names[i] = "";
                currentList.Scales[i] = "1.0";
            }

            string output = JsonConvert.SerializeObject(currentList);
            File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "currentList.txt"), output);
        }

        log("CurrentList: ");
        for (int i = 0; i < currentList.Uuids.Length; i++)
        {
            log("Uuid: " + currentList.Uuids[i] + " Name: " + currentList.Names[i] + " Scale: " + currentList.Scales[i]);
        }
    }

    void SaveCurrentList()
    {
        log("SAVING LIST FILE");
        string output = JsonConvert.SerializeObject(currentList);
        File.WriteAllText(System.IO.Path.Combine(Application.persistentDataPath, "currentList.txt"), output);
        log("SAVED");
    }



    /* ###################### WEB SOCKETS PART ######################*/
    public void CreateWebSocketSession()
    {
        log("WS: Connection with WS");
        wsClient = new WebSocket(Constants.WSHost);
        wsClient.OnOpen += OnWsOpen;
        wsClient.OnClose += OnWsClose;
        wsClient.OnMessage += OnWsMessage;
        wsClient.OnError += OnWsError;
        wsClient.Connect();
        log("WS - Connected? let hope");
    }

    public void OnWsMessage(object sender, MessageEventArgs eventArgs)
    {

        Debug.Log("WS MESSAGE: " + eventArgs.Data);
        // WSUpdate wsUpdate = JsonConvert.DeserializeObject<WSUpdate>(eventArgs.Data);

        // switch(wsUpdate.Type){
            
        //     case "model":
        //         ModelUpdate
        //         break;
            
        //     case "scale":
        //         break;
            
        //     case "light":
        //         break;

        //     case "new_image":
        //         break;

        //     case "change_image":
        //         break;

        // }
        ModelUpdate mu = JsonConvert.DeserializeObject<ModelUpdate>(eventArgs.Data);

        JObject s = new JObject();
        log("WS - WebSocket Message recieved: " + mu.Update + " " + mu.Uuid + " " + mu.Name + " " + mu.Scale + " " + mu.Target);
        
        if (mu.Update == "model")
        {
            //check if model exist locally before changing
            //if(GameObject.Find(mu.Name) != null)
            //ExecuteOnMainThread.Enqueue(() => { StartCoroutine(UpdateModel(mu.Uuid, mu.Name, mu.Scale, mu.Target)); });

            ExecuteOnMainThread.Enqueue(() =>
            {
                if (GameObject.Find(mu.Name) != null)
                    UpdateModel(mu.Uuid, mu.Name, float.Parse(mu.Scale), int.Parse(mu.Target));
            });


        }
        else if (mu.Update == "scale")
        {
            ExecuteOnMainThread.Enqueue(() => { UpdateScale(int.Parse(mu.Target), float.Parse(mu.Scale)); });
            //UpdateScale(mu.Target, mu.Scale);
        }
        else if (mu.Update == "light")
        {
            ExecuteOnMainThread.Enqueue(() => { UpdateLight(int.Parse(mu.Target)); });
        }
        else if(mu.Update == "new_image"){

            log("IMAGE: "+ mu.Uuid);

            ExecuteOnMainThread.Enqueue(() => { downloadNewScreenshot(mu.Uuid); });
        }
        else if(mu.Update == "change_image"){

            log("Update displayed image: image_index or URL");

            ExecuteOnMainThread.Enqueue(() => { changeMainImage(int.Parse(mu.Uuid)); });
            //TODO: save url, download image and add to gallery
            //TODO: call changeImage(index of last image in screeshosts_imgs)
        }
    }

    private void OnWsOpen(object sender, EventArgs eventArgs)
    {
        log("WebSocket connection OPEN.");

        //TODO: register device
        //WsSendMessage("Connection to client successful!");
    }

    private void OnWsClose(object sender, CloseEventArgs eventArgs)
    {
        log("WebSocket connection CLOSED.");
        if (!eventArgs.WasClean)
        {
            if (!wsClient.IsAlive)
            {
                log("WS: Trying to recconecct from onWSClose");
                wsClient.Connect();
            }
        }
        //wsClient.Connect();
    }

    private void OnWsError(object sender, WebSocketSharp.ErrorEventArgs eventArgs)
    {
        log("WebSocket connection ERROR: " + eventArgs.Message);
        //log("Trying to reconnect: " + eventArgs.Message);
        //wsClient.Connect();
    }

    public void WsSendMessage(string message)
    {
        try
        {
            wsClient.Send(message);
            log("WebSocket sent message: " + message);
        }
        catch (Exception e)
        {
            log("Failed to send message: " + e.ToString());
        }
    }

    /* ###################### WEB SOCKETS PART- END ######################*/

    public void customOnFound(string targetName){

        int target_id = int.Parse(targetName[targetName.Length - 1].ToString());
        target_id = target_id - 1;

        if(trackedImageTargets.Contains("ImageTarget" + target_id) == false)
            trackedImageTargets.Add("ImageTarget" + target_id);

        printList();
        log("CUSTOM FOUND: " + targetName);
        

        float scale = float.Parse(currentList.Scales[target_id]);

        GameObject imageTarget = GameObject.Find("ImageTarget" + target_id);
        GameObject child = imageTarget.transform.GetChild(0).gameObject;

        child.transform.localScale = new Vector3(0.007f * scale, 0.007f * scale, 0.007f * scale);
    }

    public void customOnLost(string targetName)
    {
        log("CUSTOM LOST: " + targetName);

        int target_id = int.Parse(targetName[targetName.Length - 1].ToString());
        target_id = target_id - 1;

        if(trackedImageTargets.Contains("ImageTarget" + target_id))
            trackedImageTargets.Remove("ImageTarget" + target_id);

        printList();

        float scale = 0.007f;
        GameObject imageTarget = GameObject.Find("ImageTarget" + target_id);

        if(imageTarget.transform.GetChild(0).gameObject != null){

            GameObject child = imageTarget.transform.GetChild(0).gameObject;
            child.transform.localScale = new Vector3(0.007f * scale, 0.007f * scale, 0.007f * scale);
        }
    }


    /* MODELS UPDATES FUNCTIONS */
    public IEnumerator GetAllModels()
    {

        log("FETCHING MODELS LIST FROM SERVER");

        //Retrieve the current list of models from the server
        UnityWebRequest www = UnityWebRequest.Get(Constants.HttpHost + "/api/meeting/" + Constants.MeetingID);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
            log("Http error when downloading the model list: " + www.error);
        else
        {
            string list = www.downloadHandler.text;
            log("RECEIVED FROM SERVER: " + list);

            ModelList ml = JsonConvert.DeserializeObject<ModelList>(list);

            log("Recieved the following list of models and targets: ");
            for (int i = 0; i < ml.Uuids.Length; i++)
                log("(Target: " + i + ", Model: " + ml.Uuids[i] + ", Name: " + ml.Names[i] + ", Scale: " + ml.Scales[i] + ")\n");

            //Download all models that we don't have already
            for (int i = 0; i < ml.Uuids.Length; i++)
            {
                if (GameObject.Find(ml.Names[i]))
                    log("MDOEL FOUND "+ ml.Names[i]);
                else
                    log("MODEL NOT FOUND" + ml.Names[i]);

                if (ml.Uuids[i] != currentList.Uuids[i] && GameObject.Find(ml.Names[i]) != null) //If we don't already have this model saved // && ml.Uuids[i] != "0"
                {
                    log("A NEW MODEL FOUND");
                    currentList.Uuids[i] = ml.Uuids[i];
                    currentList.Names[i] = ml.Names[i];
                    currentList.Scales[i] = ml.Scales[i];
                    SaveCurrentList();
                }
            }
        }

        //Set Model to corresponding image target even if we are unable to connect to the server (show stored models)
        for (int i = 0; i < currentList.Uuids.Length; i++)
        {
            if (currentList.Uuids[i] != "0") //Set model to corresponding image target, only if there is a model
            {
                GameObject target = GameObject.Find("ImageTarget" + i);
                log("Setting model: " + currentList.Uuids[i] + " to image target " + i);
                //GameObject setModel = Resources.Load<GameObject>("Models/Model" + i);

                GameObject setModel = GameObject.Find(currentList.Names[i]);

                //float new_scale = float.Parse(currentList.Scales[i]);
                GameObject newModel = Instantiate(setModel, setModel.transform.position, new Quaternion(0, 0, 0, 0), target.transform);

                //newModel.transform.position = setModel.transform.position + new Vector3(target.transform.position.x, 0.0f, target.transform.position.z);

                newModel.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
               
                newModel.SetActive(true);
            }
        }
    }
                                        
    public void UpdateModel(string modelId, string name, float scale, int target)
    {
        log("UpdateModel()");

        //Updata current list
        currentList.Uuids[target] = modelId;
        currentList.Names[target] = name;
        currentList.Scales[target] = scale.ToString();
        SaveCurrentList();

        printList();

        //Set new model to corresponding image target
        GameObject imageTarget = GameObject.Find("ImageTarget" + target);
        log("Setting model: " + modelId + " to image target " + target + " with scale " + scale);

        //remove previous model from target
        if(imageTarget.transform.childCount > 0){

            GameObject prevModel = imageTarget.transform.GetChild(0).gameObject;
            Destroy(prevModel);
        }

        //set new model on target
        GameObject setModel = GameObject.Find(name);
        //GameObject newModel = Instantiate(setModel, setModel.transform.position, setModel.transform.rotation, imageTarget.transform);
        GameObject newModel = Instantiate(setModel, setModel.transform.position, new Quaternion(0.0f, 0.0f, 0.0f, 0.0f), imageTarget.transform);

        newModel.transform.position = new Vector3(0.0f, 0.0f, 0.0f) + imageTarget.transform.position + setModel.transform.position;

        if(trackedImageTargets.Contains("ImageTarget" + target)){

            newModel.transform.localScale = new Vector3(0.007f * scale, 0.007f * scale, 0.007f * scale);
            newModel.SetActive(true);
        }
    }

    public void UpdateScale(int target, float scale)
    {
        log("Updating scale on target: " + target + " to " + scale);

        GameObject imageTarget = GameObject.Find("ImageTarget" + target);
        GameObject child = imageTarget.transform.GetChild(0).gameObject;
        child.transform.localScale = new Vector3(0.007f * scale, 0.007f * scale, 0.007f * scale);

        currentList.Scales[target] = scale.ToString();
        SaveCurrentList();
    }

    public void UpdateLight(int angle)
    {
        log("NEW LIGHT VALUE: " + angle);
        //30,-30,0 = sunrise
        //90,-30,0 = High noon
        //180,-30,0 = sunset
        //-90,-30,0 = Midnight
        directional_light.transform.localEulerAngles = new Vector3(angle, -30, 0);
    }


    /* GALLERY FUNCTIONS */
    public void downloadNewScreenshot(string url){
        StartCoroutine(this.GetComponent<UIHandler>().downloadScreenshot(url, true));
    }

     public void changeMainImage(int image_index){

        this.GetComponent<UIHandler>().changeMainImage(image_index);
    }

    public void triggerScreenshot(){
        
        StartCoroutine(takeScreenshot());
    }
    public IEnumerator takeScreenshot(){

        log("TAKING SCREENSHOT");

        var timestamp = DateTime.Now.ToString("yyyy-MM-dd_hh_mm_ss");
        var file_name = "screenshot_" + timestamp + ".png";
        var file_path = System.IO.Path.Combine(Application.persistentDataPath, file_name);

        if (File.Exists(file_path))
            File.Delete(file_path);

        MainCanvas.SetActive(false);
        if (Application.platform == RuntimePlatform.IPhonePlayer)
            ScreenCapture.CaptureScreenshot(file_name);
        else
            ScreenCapture.CaptureScreenshot(file_path);

        yield return new WaitForSeconds(1);

        MainCanvas.SetActive(true);
        StartCoroutine( shareScreenshot(file_name, file_path) );
    }

    private IEnumerator shareScreenshot(string file_name, string file_path){

        byte[] image_data = File.ReadAllBytes(file_path);

        WWWForm postForm = new WWWForm();
        postForm.AddField("param_name", "param_value");
        postForm.AddField("device", "imran_mac");
        postForm.AddBinaryData("screenshot_file", image_data, file_name, "image/png");

        WWW upload = new WWW(Constants.HttpHost + "/screenshot", postForm);
        
        log("SENDING SCREENSHOT TO SERVER");

        yield return upload;

        if (upload.error == null) log("SCREENSHOT SENT");
        else log("ERRO DURING SCREENSHOT UPLOAD\n" + upload.error);
    }


    /* UTILS */
    public void log(string msg){
        Debug.Log("DEBUG - " + msg);
    }

    public void printList(){

        log("TRACKED LIST: " + trackedImageTargets.Count);
        trackedImageTargets.ForEach(delegate(String targetName)
        {
            log(targetName);
        });
    }
}

