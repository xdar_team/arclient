public class ImageUpdate{

    public string update {get; set;}
    public int image_index {get; set;}

    public ImageUpdate(string type, int image_index){

        this.update = type;
        this.image_index = image_index;
    }
}